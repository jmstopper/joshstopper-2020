<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77151017-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-77151017-1');
    </script>

    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">

    <link rel="preload" href="<?php echo \Stratum\Paths\assetURL('fonts/brandon-reg.woff2'); ?>" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?php echo \Stratum\Paths\assetURL('fonts/brandon-med.woff2'); ?>" as="font" type="font/woff2" crossorigin>

    <?php wp_head(); ?>

    <meta name="theme-color" content="#ff1493"/>
</script>
</head>

<body <?php body_class(); ?>>
    <?php echo \Stratum\render('assets/components/skip-link'); ?>
    <?php echo \Stratum\render('assets/components/header'); ?>
