<?php

$post_type = get_post_type_object(get_post_type());

the_post_navigation([
    'prev_text' => __('Previous ' . $post_type->labels->singular_name, 'stratum'),
    'next_text' => __('Next ' . $post_type->labels->singular_name, 'stratum'),
]);
