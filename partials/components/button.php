<?php

// Placeholder for all the attributes to be output
$atts_string = [];

// Check if we need to add any attributes to the string
if (STM::is_file_url($args['atts']['href'])) {
    $args['atts']['download'] = $args['text'];
}

if (array_key_exists('class', $args['atts']) !== true) {
    $args['atts']['class'] = 'button';
}

if (array_key_exists('target', $args['atts']) && $args['atts']['target'] === '_blank') {
    $args['atts']['rel'] = "noopener noreferrer";
}

// Turn each of the key value pairs into an array we can implode
foreach ($args['atts'] as $key => $value) {
    if ($key === 'href') {
        $atts_string[] = esc_attr($key) . '="' . esc_url($value) . '"';
    } else {
        $atts_string[] = esc_attr($key) . '="' . esc_attr($value) . '"';
    }
}

?><a <?php echo implode(' ', $atts_string); ?>>
    <?php echo esc_html($args['text']); ?>
</a>