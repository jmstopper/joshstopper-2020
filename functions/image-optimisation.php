<?php

add_filter('wp_get_attachment_image_attributes', function (array $attr, object $attachment, $size) {
    // For an education on responsive images, check this link
    // https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images

    // https://developer.wordpress.org/reference/hooks/wp_get_attachment_image_attributes/

    // Don't do anything in the admin area
    if (is_admin()) {
        return $attr;
    }

    // If $size is an array, width and height were passed
    // and not a defined image size
    if (is_array($size)) {
        $attr['sizes'] = $size[0] . 'px';
    } else {
        $partial = \Stratum\render\last();

        if ($partial['path'] === 'assets/components/card') {
            $attr['sizes'] = '460px';
        }
    }

    return $attr;
}, 25, 3);
