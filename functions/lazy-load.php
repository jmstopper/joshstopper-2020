<?php

add_filter('wp_get_attachment_image_attributes', function (array $attributes) {
    if (is_admin()) {
        return $attributes;
    }

    $attributes['loading'] = 'lazy';

    return $attributes;
}, 10);
