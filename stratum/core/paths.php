<?php

namespace Stratum\Paths;

/**
 * Take a path and turn it in to a legitimate file path. Allows for a child
 * theme to implement a new version of the partial
 * @param string $path
 * @return string
 */
function resolve(string $path): string
{
    $path = \Stratum\Paths\themePath($path);

    if (is_dir($path)) {
        $path = $path . '/index.php';
    }

    $pathinfo = pathinfo($path);

    // Check if there is an extension.
    // If note add one
    if (isset($pathinfo['extension']) !== true) {
        $path = $path . '.php';
    }

    return $path;
}

function assetContent(string $path, string $type = 'string')
{
    $content = trim(file_get_contents(\Stratum\Paths\assetPath($path)));

    if ($type === 'json') {
        return json_decode($content, true);
    }

    return $content;
}

function assetPath(string $asset, bool $manifest = false): string
{
    if ($manifest === true) {
        $asset = extractAsset($asset);
    }

    return \Stratum\Paths\themePath(
        \Stratum\Paths\assetDir() . $asset
    );
}

function assetURL(string $asset, bool $manifest = false): string
{
    if ($manifest === true) {
        $asset = extractAsset($asset);
    }

    return \Stratum\Paths\themeURL(
        \Stratum\Paths\assetDir() . $asset
    );
}

function extractAsset(string $asset): string
{
    $path = \Stratum\Paths\assetContent('manifest.json', 'json');
    $parts = explode('/', $asset);

    foreach ($parts as $key => $part) {
        $path = $path[$part];
    }

    return $path;
}

function assetDir(): string
{
    return 'assets/';
}

function themeURL(string $path = ''): string
{
    // https://developer.wordpress.org/reference/functions/get_stylesheet_directory_uri/
    // Supports child theme overriding
    return get_stylesheet_directory_uri() . '/' . $path;
}

function themePath(string $path = ''): string
{
    // https://developer.wordpress.org/reference/functions/get_theme_file_path/
    // Supports child theme overriding
    if ($path !== '') {
        return get_theme_file_path() . '/' . $path;
    }

    return get_theme_file_path();
}

function pathToURL(string $path): string
{
    $path = str_replace(\Stratum\Paths\themePath() . '/', '', $path);
    return \Stratum\Paths\themeURL($path);
}
