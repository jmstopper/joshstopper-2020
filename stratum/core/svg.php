<?php

namespace Stratum;

/**
 * Return the content of an SVG file in the assets directory
 * @param string $name
 * @param array $args
 * @return string
 */
function svg(string $name, array $args = []): string
{
    // How to edit an SVG in PHP
    // https://stackoverflow.com/questions/41264017/php-svg-editing
    // https://stackoverflow.com/questions/18758101/domdocument-add-attribute-to-root-tag

    // Merge attributes
    $args = wp_parse_args($args, [
        'wrapped' => false,
        'title' => '',
        'description' => '',
        'id' => uniqid()
    ]);

    // Create a new instance of DOMDocument
    $doc = new \DOMDocument;

    // Load in the SVG
    $doc->loadXML(
        // Get the content of the svg file
        \Stratum\Paths\assetContent('svgs/' . $name . '.svg')
    );

    // set a role attribute on the SVG element
    $doc->documentElement->setAttribute('role', 'img');

    // Is there a title
    if ($args['title'] !== '') {
        $titleID = 'title-' . $args['id'];

        $labelled = [];
        $labelled[] = $titleID;

        // Add a title
        $title = $doc->createElement('title', esc_attr($args['title']));
        $title->setAttribute('id', 'title-' . $args['id']);

        // Append it to the SVG
        $doc->firstChild->appendChild($title);

        // Is there a description
        if ($args['description'] !== '') {
            $descriptionID = 'description-' . $args['id'];
            $labelled[] = $descriptionID;

            // Add a description
            $description = $doc->createElement('description', esc_attr($args['description']));
            $description->setAttribute('id', $descriptionID);

            // Append it to the SVG
            $doc->firstChild->appendChild($description);
        }

        // Add the attributes to the SVG
        $doc->documentElement->setAttribute('aria-labelledby', implode(' ', $labelled));
    }

    // output the svg markup and strip the xml doctype declaration
    // https://stackoverflow.com/questions/5706086/php-domdocument-output-without-xml-version-1-0-encoding-utf-8/17362447
    $svg = $doc->saveXML($doc->documentElement);

    if ($args['wrapped'] === true) {
        $svg = '<span class="o-icon o-icon--' . esc_attr($name) . '">' . $svg . '</span>';
    }

    return $svg;
}

/**
 * Build the path to the SVG asset in the theme
 * @param string $name
 * @return string
 */
function svgPath(string $name): string
{
    return \Stratum\Paths\assetPath('svgs/' . $name . '.svg');
}

/**
 * Build the URL for the SVG
 * @param string $name
 * @return string
 */
function svgURL(string $name): string
{
    return \Stratum\Paths\assetURL('svgs/' . $name . '.svg');
}
