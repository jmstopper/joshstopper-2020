<?php

namespace Stratum;

function isFileURL(string $url): bool
{
    $extensions = ['pdf', 'doc', 'jpg', 'png'];
    $pathInfo = pathinfo($url);

    if (isset($pathInfo['extension']) && in_array($pathInfo['extension'], $extensions)) {
        return true;
    }

    return false;
}

function startsWith(string $haystack, string $needle): bool
{
    // search backwards starting from haystack length characters from the end
    return $needle === '' || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith(string $haystack, string $needle): bool
{
    // search forward starting from end minus needle length characters
    if ($needle === '') {
        return true;
    }

    $diff = \strlen($haystack) - \strlen($needle);
    return $diff >= 0 && strpos($haystack, $needle, $diff) !== false;
}
