<?php
namespace Stratum\WordPress;

class ThemeSupport
{
    public static function init()
    {
        // Define WordPress content width to limit size of elements output in wysiwyg
        if (defined('STM_CONTENT_WIDTH') && !isset($content_width)) {
            global $content_width;
            $content_width = STRATUM_CONTENT_WIDTH;
        }

        add_action('after_setup_theme', [__CLASS__, 'titleTag']);
        add_action('after_setup_theme', [__CLASS__, 'html5']);
        add_action('after_setup_theme', [__CLASS__, 'postFormats']);
    }

    public static function titleTag()
    {
        add_theme_support('title-tag');
    }

    /**
     * Registers theme support for a given feature
     * @return void
     */
    public static function html5()
    {
        add_theme_support('html5', [
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
        ]);
    }

    /**
     * Register theme support for post formats
     * @link https://codex.wordpress.org/Post_Formats
     * @return void
     */
    public static function postFormats()
    {
        if (defined('STRATUM_POST_FORMATS') && !empty(STRATUM_POST_FORMATS)) {
            add_theme_support('post-formats', STRATUM_POST_FORMATS);
        }
    }
}
