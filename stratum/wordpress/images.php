<?php

namespace Stratum\WordPress;

class Images
{

    public static function init(): void
    {
        add_action('after_setup_theme', [__CLASS__, 'imageSizes']);
        add_filter('image_size_names_choose', [__CLASS__, 'imageSizeNamesChoose']);
    }

    /**
     * Register the image sizes we want to use in the theme
     * @return void
     */
    public static function imageSizes(): void
    {
        if (defined('STRATUM_IMAGES') && is_array(STRATUM_IMAGES)) {
            add_theme_support('post-thumbnails');
            set_post_thumbnail_size(150, 150, true);

            foreach (STRATUM_IMAGES as $key => $size) {
                $crop = false;

                if (isset($size['crop']) && is_bool($size['crop'])) {
                    $crop = $size['crop'];
                }

                add_image_size(
                    $size['name'],
                    $size['width'],
                    $size['height'],
                    $crop
                );
            }
        }
    }

    /**
     * Add the custom image sizes to the size selector in wordpress
     * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/image_size_names_choose
     * @param array $sizes
     * @return type array
     */
    public static function imageSizeNamesChoose(array $sizes): array
    {
        if (defined('STRATUM_IMAGES') && is_array(STRATUM_IMAGES)) {
            foreach (STRATUM_IMAGES as $key => $size) {
                $sizes[$size['name']] = ucfirst(
                    str_replace(
                        '_',
                        ' ',
                        $size['name']
                    )
                );
            }
        }

        return $sizes;
    }
}
