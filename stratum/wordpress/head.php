<?php

namespace Stratum\WordPress;

class Head
{
    public static function init(): void
    {
        // Disable XML-RPC RSD link
        // https://developer.wordpress.org/reference/functions/rsd_link/
        remove_action('wp_head', 'rsd_link');

        // Remove WordPress version number
        // https://developer.wordpress.org/reference/functions/wp_generator/
        remove_action('wp_head', 'wp_generator');

        // Remove wlwmanifest link
        // https://developer.wordpress.org/reference/functions/wlwmanifest_link/
        remove_action('wp_head', 'wlwmanifest_link');

        // Remove shortlink
        // https://developer.wordpress.org/reference/functions/wp_shortlink_wp_head/
        remove_action('wp_head', 'wp_shortlink_wp_head');

        // Attempt to disable wp-embeds
        add_action('wp_footer', [__CLASS__, 'footer']);

        // DO we need to cleanup emoji code
        if (defined('STRATUM_EMOJI') && STRATUM_EMOJI === false) {
            // remove all actions related to emojis
            add_action('init', [__CLASS__, 'disableWPEmoji']);

            // Remove DNS prefetch of emoji code
            add_action('wp_resource_hints', [__CLASS__, 'resourceHints'], 10, 2);

            // filter to remove TinyMCE emojis
            add_filter('tiny_mce_plugins', [__CLASS__, 'disableEmojiTinymce']);
        }
    }

    /**
     * Attempt to disable wp-embed script from being output
     * @return void
     */
    public static function footer(): void
    {
        wp_deregister_script('wp-embed');
    }

    public static function resourceHints(array $urls, $relation_type): array
    {

        for ($i = 0; $i < count($urls); $i++) {
            // Removes <link rel="dns-prefetch" href="//s.w.org"> which is
            // preloaded for emojis...
            if (strpos($urls[$i], 'emoji') !== false) {
                unset($urls[$i]);
                break;
            }
        }

        return $urls;
    }

    /**
     * Remove emoji support from WordPress front end
     * @return void
     */
    public static function disableWPEmoji(): void
    {
        remove_action('admin_print_styles', 'print_emoji_styles');
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_filter('comment_text_rss', 'wp_staticize_emoji');
        remove_filter('the_content_feed', 'wp_staticize_emoji');
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    }

    /**
     * Remove emoji support from WordPress back end
     * @param array $plugins
     * @return array
     */
    public static function disableEmojiTinymce(array $plugins = []): array
    {
        return array_diff(
            $plugins,
            ['wpemoji']
        );
    }
}
