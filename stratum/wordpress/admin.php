<?php

namespace Stratum\WordPress;

class Admin
{
    public static function init(): void
    {
        add_filter('upload_mimes', [__CLASS__, 'uploadMimes']);
        add_filter('login_errors', [__CLASS__, 'loginErrors']);
    }

    /**
     * Allow extra mime types to be uploaded to the WordPress media library
     * @param array $mimes
     * @return array
     */
    public static function uploadMimes(array $mimes): array
    {
        if (defined('STRATUM_MIMES') && is_array(STRATUM_MIMES)) {
            foreach (STRATUM_MIMES as $key => $value) {
                $mimes[$key] = $value;
            }
        }

        return $mimes;
    }

    /**
     * Obscure the login failure message. Should probs be moved to a plugin
     * @return string
     */
    public static function loginErrors(): string
    {
        return __(
            "Sorry: We don't recognise that user name and password.",
            'stratum'
        );
    }
}
