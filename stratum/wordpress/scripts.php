<?php
namespace Stratum\WordPress;

class Scripts
{
    public static function init()
    {
        // Add the styles and scripts
        add_action('wp_enqueue_scripts', [__CLASS__, 'register'], 15);
        add_action('wp_enqueue_scripts', [__CLASS__, 'enqueue'], 16);

        // Remove the version strings
        add_filter('style_loader_src', [__CLASS__, 'removeVersionString'], 9999, 2);
        add_filter('script_loader_src', [__CLASS__, 'removeVersionString'], 9999, 2);

        // Change the type of the theme js
        add_filter('script_loader_tag', [__CLASS__, 'module'], 10, 3);

        add_action('enqueue_block_editor_assets', [__CLASS__, 'gutenbergStyles']);
    }

    public static function gutenbergStyles():void
    {
        wp_enqueue_style('stratum-styles-gutenberg', \Stratum\Paths\assetURL('styles/gutenberg.css', true), []);
    }

    /**
     * Registers styles and scripts required by our theme
     * @return void
     */
    public static function register()
    {
        // Placeholder for the dependencies of our main JS file
        $scriptDependencies = [];

        /*
        * If we are in wp-admin, load the default stylesheet, otherwise load
        * up our custom stylesheet. This probs has to change with Gutenberg on
        * the horizon
        */
        wp_register_style(
            'stratum-style-core',
            is_admin() ?
                get_stylesheet_uri() :
                \Stratum\Paths\assetURL('styles/styles.css', true)
        );

        if (defined('STRATUM_GOOGLE_API_KEY') &&  STRATUM_GOOGLE_API_KEY !== '') {
            wp_register_script('stratum-script-map', esc_url('https://maps.googleapis.com/maps/api/js?key=' . STRATUM_GOOGLE_API_KEY), [], '', true);
        }

        if (defined('STRATUM_REQUIRE_JQUERY') && STRATUM_REQUIRE_JQUERY === true) {
            $scriptDependencies[] = 'jquery-core';
        }

        // Register the main script file
        wp_register_script(
            'stratum-script-core',
            \Stratum\Paths\assetURL('scripts/scripts.js', true),
            $scriptDependencies,
            '',
            true
        );
    }

    /**
     * Enqueue scripts that we have registered
     * @return void
     */
    public static function enqueue()
    {
        if (STRATUM_AJAX === true) {
            // You should probably add a nonce here for each ajax action taken
            // https://codex.wordpress.org/Function_Reference/wp_create_nonce
            wp_localize_script('stratum-script-core', 'wp_vars', [
                'ajax_url'  => esc_url(admin_url('admin-ajax.php')),
                'home_url'  => esc_url(home_url()),
            ]);
        }

        if (wp_script_is('stratum-script-map', 'registered') && STRATUM_MAPS === true) {
            wp_enqueue_script('stratum-script-map');
        }

        if (wp_style_is('stratum-style-core', 'registered')) {
            wp_enqueue_style('stratum-style-core');
        }

        if (wp_script_is('stratum-script-core', 'registered')) {
            wp_enqueue_script('stratum-script-core');
        }
    }

    public static function removeVersionString($src, $handle): string
    {
        if (strpos($src, 'assets/scripts') !== false || strpos($src, 'assets/styles') !== false) {
            $src = remove_query_arg('ver', $src);
        }

        return $src;
    }

    public static function module(string $tag, string $handle, string $src): string
    {
        $search = "type='text/javascript'";
        // Check if we are working with the main scripts file
        if (strpos($src, 'assets/scripts/scripts') !== false) {
            return str_replace($search, 'type="module" async defer', $tag);
        }

        return $tag;
    }
}
