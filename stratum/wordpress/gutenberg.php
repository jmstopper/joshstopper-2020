<?php

namespace Stratum\WordPress;

class Gutenberg
{
    public static function init(): void
    {
        // https://developer.wordpress.org/block-editor/developers/themes/theme-support/#responsive-embedded-content
        add_theme_support('responsive-embeds');

        // https://developer.wordpress.org/block-editor/developers/themes/theme-support/#wide-alignment
        // add_theme_support('align-wide');

        // https://developer.wordpress.org/block-editor/developers/themes/theme-support/#disabling-custom-colors-in-block-color-palettes
        add_theme_support('disable-custom-colors');

        // https://developer.wordpress.org/block-editor/developers/themes/theme-support/#disabling-custom-font-sizes
        global $STRATUM_COLORS;
        add_theme_support('editor-color-palette', $STRATUM_COLORS);

        // https://developer.wordpress.org/block-editor/developers/themes/theme-support/#disabling-custom-font-sizes
        // This removes the custom input field for users to specify a font size.
        // Well its meant to anyway. There is a bug in core that means custom
        // still appears as an option even if the input has been removed.
        add_theme_support('disable-custom-font-sizes');

        // https://developer.wordpress.org/block-editor/developers/themes/theme-support/#block-font-sizes
        global $STRATUM_FONT_SIZES;
        add_theme_support('editor-font-sizes', $STRATUM_FONT_SIZES);
    }

    public static function classes(array $args, array $block): array
    {
        if (!empty($block['align'])) {
            $args['class'] = 'align' . $block['align'];
        }

        return $args;
    }
}
