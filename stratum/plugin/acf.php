<?php

namespace Stratum\Plugin;

class ACF
{
    public static function init(): void
    {
        add_action('acf/init', [__CLASS__, 'google']);
        add_action('acf/init', [__CLASS__, 'optionPages']);
    }

    public static function google(): void
    {
        // Register a google maps api key
        if (function_exists('acf_update_setting') && defined('STM_GOOGLE_API_KEY') && STRATUM_GOOGLE_API_KEY != '') {
            acf_update_setting('google_api_key', STRATUM_GOOGLE_API_KEY);
        }
    }

    /**
     * Provide ACF with a Google maps API key if it has been configured
     * @return void
     */
    public static function optionPages(): void
    {
        /**
         * Add an options page to the theme for global settings.
         * Runs without needing a hook
         */
        if (function_exists('acf_add_options_page')) {
            // Global Option pages
            if (defined('STRATUM_ACF_OPTIONS_PAGES') && is_array(STRATUM_ACF_OPTIONS_PAGES)) {
                acf_add_options_page();

                foreach (STRATUM_ACF_OPTIONS_PAGES as $key => $page) {
                    acf_add_options_sub_page($page);
                }
            }

            // Post type option pages
            if (defined('STRATUM_ACF_POST_OPTIONS') && is_array(STRATUM_ACF_POST_OPTIONS)) {
                foreach (STRATUM_ACF_POST_OPTIONS as $key => $page) {
                    $parent = 'edit.php';

                    if ($page['type'] !== 'post') {
                        $parent = $parent . '?post_type=' . $page['type'];
                    }

                    acf_add_options_sub_page([
                        'title'      => esc_html($page['name']),
                        'parent'     => $parent,
                        'capability' => 'manage_options'
                    ]);
                }
            }
        }
    }
}
