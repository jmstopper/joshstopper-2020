<?php

namespace Stratum\Plugin;

class YoastSEO
{
    public static function init(): void
    {
        add_filter('wpseo_metabox_prio', [__CLASS__, 'wpseoMetaboxPriority']);
        add_theme_support('yoast-seo-breadcrumbs');
    }

    /**
     * Make the Yoast SEO meta box appear as low as possible on the post edit screen
     * @param string $pri
     * @return string
     */
    public static function wpseoMetaboxPriority(string $pri): string
    {
        return 'low';
    }
}
