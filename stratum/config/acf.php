<?php
/**
 * Define options pages for ACF
 *
 * Supports a string or an array of options. Review link below
 *
 * @link https://www.advancedcustomfields.com/resources/acf_add_options_sub_page/
 */
// const STRATUM_ACF_OPTIONS_PAGES = [
//     'General',
// ];

// const STRATUM_ACF_POST_OPTIONS = [
//     [
//         'type' => 'post',
//         'name' => 'Post options'
//     ]
// ];
