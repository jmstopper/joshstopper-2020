<?php
/**
 * Define the images sizes you would like available
 */
const STRATUM_IMAGES = [
    [
        'name'      => 'super',
        'width'     => '1500',
        'height'    => '1500',
        'crop'      => false
    ],
    [
        'name'      => 'super_duper',
        'width'     => '2000',
        'height'    => '2000',
        'crop'      => false
    ],
];
