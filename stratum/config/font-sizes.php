<?php

$STRATUM_FONT_SIZES = [
    [
        'name' => __('Small', 'stratum'),
        'size' => 12,
        'slug' => 'small'
    ],
    [
        'name' => __('Normal', 'stratum'),
        'size' => 16,
        'slug' => 'normal'
    ],
    [
        'name' => __('Large', 'stratum'),
        'size' => 24,
        'slug' => 'large'
    ],
];
