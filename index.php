<?php

get_header();

if (have_posts()) {
    global $post;

    $title = is_home() ? get_the_title(get_option('page_for_posts')) : post_type_archive_title('', false);

    if (is_search()) {
        $title = __('Search', 'stratum');
    }

    // A placeholder to put all our content in
    $myPosts = [];

    while (have_posts()) {
        the_post();
        $myPosts[] = $post;
    }

    echo \Stratum\render(
        'assets/components/main',
        \Stratum\render('assets/components/article', [
            'heading' => $title,
            'content' => \Stratum\render('assets/components/cards', [
                'cards' => $myPosts
            ])
        ])
    );
} else {
    echo \Stratum\render(
        'assets/components/main',
        \Stratum\render('partials/wordpress/content-none')
    );
}

get_footer();
