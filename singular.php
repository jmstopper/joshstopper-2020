<?php

get_header();

if (have_posts()) {
    while (have_posts()) {
        the_post();

        echo \Stratum\render(
            'assets/components/main',
            \Stratum\render('assets/components/article', [
                'heading' => get_the_title(),
                'content' => apply_filters('the_content', get_the_content())
            ])
        );
    }
} else {
    echo \Stratum\render(
        'assets/components/main',
        \Stratum\render('partials/wordpress/content-none')
    );
}

get_footer();
