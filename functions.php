<?php
// Import Stratum
require 'stratum/stratum.php';

// Everything else
require 'functions/image-optimisation.php';
require 'functions/js-folio.php';
require 'functions/lazy-load.php';
