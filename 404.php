<?php

get_header();

echo \Stratum\render(
    'assets/components/main',
    \Stratum\render('assets/components/article', [
        'heading' => __('404', 'stratum'),
        'content' => \Stratum\render('partials/wordpress/content-none')
    ])
);

get_footer();
