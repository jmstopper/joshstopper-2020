<header class="header">
    <a class="header__logo" href="<?php echo esc_url(get_home_url()); ?>">
        <?php echo \Stratum\svg('logo-type', [
            'title' => get_bloginfo('name')
        ]); ?>
    </a>

    <nav class="header__navigation" role="navigation" aria-label="<?php esc_attr_e('Header navigation', 'stratum'); ?>">
        <?php wp_nav_menu([
            'theme_location'  => 'header',
            'depth'           => 0,
            'container'       => '',
            'container_class' => '',
            'menu_class'      => 'header__menu',
            'fallback_cb'     => false,
        ]);?>
    </nav>
</header>
