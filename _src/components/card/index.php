<<?php echo $args['el']; ?> class="c-card">
    <div class="c-card__inner">
        <?php if (!empty($args['heading'])) { ?>
            <<?php echo $args['headingEl']; ?> class="c-card__heading">
                <?php if (!empty($args['link'])) { ?>
                    <a href="<?php echo esc_url($args['link']); ?>">
                <?php } ?>

                <?php echo $args['heading']; ?>

                <?php if (!empty($args['link'])) { ?>
                    </a>
                <?php } ?>
            </<?php echo $args['headingEl']; ?>>
        <?php } ?>

        <?php if (!empty($args['meta'])) { ?>
            <div class="c-card__meta">
                <?php echo $args['meta']; ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['content'])) { ?>
            <div class="c-card__content">
                <?php echo $args['content']; ?>
            </div>
        <?php } ?>
    </div>

    <?php if (!empty($args['media'])) { ?>
        <div class="c-card__media img-fit">
            <?php echo $args['media']; ?>
        </div>
    <?php } ?>
</<?php echo $args['el']; ?>>
