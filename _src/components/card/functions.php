<?php

add_filter('stratum/render/assets/components/card', function ($args): array {

    if ($args instanceof WP_Post) {
        $postID = $args->ID;

        $args = [
            'heading' => get_the_title($postID),
            'link' => get_the_permalink($postID),
            'media' => get_the_post_thumbnail($postID, 'large'),
            'meta' => '',
            'el' => 'article',
            'headingEl' => 'h2'
        ];

        if (get_post_type($postID) === 'post') {
            $args['meta'] = get_the_date('', $postID);
        }
    } else {
        if (!empty($args['image']['id'])) {
            $args['media'] = wp_get_attachment_image($args['image']['id'], 'large');
            unset($args['image']);
        }
    }

    return wp_parse_args($args, [
        'heading' => '',
        'content' => '',
        'media' => '',
        'meta' => '',
        'el' => 'div',
        'headingEl' => 'div',
    ]);
});
