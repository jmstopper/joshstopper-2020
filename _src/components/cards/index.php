<?php

$class = ['cards'];

if (!empty($args['class'])) {
    $class[] = $args['class'];
}

?><div class="<?php echo esc_attr(implode(' ', $class)); ?>">
    <?php foreach ($args['cards'] as $key => $card) { ?>
        <?php echo \Stratum\render('assets/components/card', $card); ?>
    <?php } ?>
</div>
