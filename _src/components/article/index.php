<article id="post-<?php the_ID(); ?>" <?php post_class('article'); ?>>
    <?php echo \Stratum\render('assets/components/entry', [
        'heading' => $args['heading'],
        'content' => $args['content']
    ]); ?>
</article>
