<header class="entry-header">
    <h1 class="entry-title"><?php echo $args['heading']; ?></h1>
</header>

<div class="entry-content">
    <?php echo $args['content']; ?>
</div>
