import '../components/_components';
import * as basicScroll from 'basicscroll'
import 'instant.page';

basicScroll.create({
    elem: document.querySelector('html'),
    from: 'top-top',
    to: 'bottom-bottom',
    props: {
        '--scale': {
            from: '0',
            to: '1'
        }
    }
}).start();

if(window.matchMedia('(min-width: 992px)').matches) {
    const cards = document.querySelectorAll('.c-card');

    for(const card of cards) {
        basicScroll.create({
            elem: card,
            direct: true,
            from: 'top-bottom',
            to: 'top-middle',
            props: {
                '--transform': {
                    from: '150px',
                    to: '0'
                },
            }
        }).start();
    }
}
