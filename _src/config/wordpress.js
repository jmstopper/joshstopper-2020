export default {
    "Theme Name": "Josh Stopper 2020",
    "Theme URI": "https://bitbucket.org/jmstopper/joshstopper-2020",
    "Author": "Josh Stopper",
    "Author URI": "https://joshstopper.com.au",
    "Description": "Custom theme for joshstopper.com.au",
    "Licence": "GNU General Public License v2 or later",
    "Text Domain": "js",
    "Tags": "",
}
