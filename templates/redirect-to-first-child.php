<?php
/**
 * Template Name: Redirect to first child
 */
if ($children = get_pages("child_of=" . $post->ID . "&sort_column=menu_order")) {
    wp_redirect(get_permalink($children[0]->ID));
} else {
    wp_redirect(get_home_url());
}
